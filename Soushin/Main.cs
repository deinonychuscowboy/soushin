using System;
using Gtk;

namespace Soushin
{
	/// <summary>
	/// A small class to keep track of how many windows are open over the lifetime of the application, basically an inverse semaphore that exits the application when it hits 0
	/// </summary>
	public static class WindowManager
	{
		private static int count = 0;
		/// <summary>
		/// Register a new window opening
		/// </summary>
		public static void Register()
		{
			WindowManager.count++;
		}
		/// <summary>
		/// Deregister a window when it closes
		/// </summary>
		public static void Deregister()
		{
			WindowManager.count--;
			if (count == 0) Application.Quit();
		}
	}
	class MainClass
	{
		public static void Main(string[] args)
		{
			Application.Init();
			MainWindow win = new MainWindow();
			WindowManager.Register();
			win.Show();
			Application.Run();
		}
	}
}
