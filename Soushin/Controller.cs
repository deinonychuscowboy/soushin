using System;
using System.Xml.Serialization;
using System.IO;

namespace Soushin
{
	/// <summary>
	/// Controller class for one window only
	/// </summary>
	public class WindowController
	{
		/// <summary>
		/// The name of the currently open file, if any
		/// </summary>
		private string filename;
		/// <summary>
		/// The raw entries deserialized from the file
		/// </summary>
		private Entry[] loadedFile;

		/// <summary>
		/// Utility class to handle XML serialization - not static so that two windows can serialize at the same time
		/// </summary>
		private readonly XmlSerializer x;
		/// <summary>
		/// Utility random number generator for scrambling questions
		/// </summary>
		private readonly Random r;

		/// <summary>
		/// True if a file is loaded
		/// </summary>
		public bool FileLoaded { get; private set; }
		/// <summary>
		/// The number of entries in the current file (not incluing any changes not yet saved)
		/// </summary>
		public int CurrentLength { get { return this.loadedFile.Length; } }

		public WindowController()
		{
			this.FileLoaded = false;
			this.filename = "";
			this.loadedFile = new Entry[0];
			this.x = new XmlSerializer(typeof(Entry[]));
			this.r = new Random();
		}
		/// <summary>
		/// Saves the data model to a specified file, or the currently open file if one is open
		/// </summary>
		/// <param name="filename">The filename to save to, optional</param>
		public void Save(string filename = null)
		{
			if (filename == null)
			{
				filename = this.filename;
			}
			else
			{
				this.filename = filename;
				FileLoaded = true;
			}
			using (StreamWriter s = new StreamWriter(filename, false))
			{
				x.Serialize(s, this.loadedFile);
			}
		}
		/// <summary>
		/// Loads a data model from the specified filename
		/// </summary>
		/// <param name="filename">The file to load from</param>
		public void Load(string filename)
		{
			using (StreamReader s = new StreamReader(filename))
			{
				this.loadedFile = (Entry[])x.Deserialize(s);
			}
			this.filename = filename;
			this.FileLoaded = true;
		}
		/// <summary>
		/// Create a new Entry instance in the model
		/// </summary>
		/// <param name="ch">The change record which defines the new values to use</param>
		/// <param name="newindex">The index of the new entry</param>
		private void Create(Change ch, int newindex)
		{
			this.loadedFile[newindex] = new Entry(ch.Newkey, ch.Newval, false);
		}
		/// <summary>
		/// Update an Entry instance
		/// </summary>
		/// <param name="old">The old entry to update</param>
		/// <param name="ch">The change record describing the new values</param>
		/// <param name="newindex">The new index to store the entry at</param>
		private void Update(Entry old, Change ch, int newindex)
		{
			this.loadedFile[newindex] = new Entry(ch.Newkey, ch.Newval, old.Hidden);
		}
		/// <summary>
		/// Process a list of change records given to us by the UI based on user actions
		/// </summary>
		/// <param name="datas">The changes to process</param>
		/// <param name="adds">Number of additions</param>
		/// <param name="dels">Number of deletions</param>
		public void ProcessChanges(Change[] datas, int adds, int dels)
		{
			Entry[] old = this.loadedFile;
			this.loadedFile = new Entry[this.loadedFile.Length + adds - dels]; // reinitialize the data model to the new size
			int next = 0;
			for (int i = 0; i < datas.Length; i++)
			{
				if (datas[i].Deleted
				    || datas[i].Added
						&& datas[i].Newkey == null
						&& datas[i].Newval == null
					)
				{
					continue; // skip anything that has been deleted, or has been added and has no values
				}
				else if (datas[i].Added) // create new
				{
					this.Create(datas[i], next++);
				}
				else // update old
				{
					this.Update(old[datas[i].Index.GetValueOrDefault()], datas[i], next++);
				}
			}
		}
		/// <summary>
		/// Get the question/answer pairs as change records for use in the UI.
		/// </summary>
		/// <param name="all">True to get all questions, false to omit ones that have previously been hidden.</param>
		/// <param name="scramble">True to randomize the order, false to use the user-specified order.</param>
		/// <returns>An array of change records corresponding to the un-changed state of the file, ready for changes to be made</returns>
		public Change[] Get(bool all, bool scramble = true)
		{
			Change[] data = new Change[this.loadedFile.Length];
			for (int i = 0; i < this.loadedFile.Length; i++) // create the change records
			{
				if (all || !this.loadedFile[i].Hidden)
				{
					data[i] = new Change(i, this.loadedFile[i].Key, this.loadedFile[i].Value);
				}
				else
				{
					data[i] = new Change(i);
				}
			}
			if (scramble) // scramble the change records
			{
				for (int t = 0; t < this.loadedFile.Length; t++)
				{
					Change tmp = data[t];
					int random = this.r.Next(0, this.loadedFile.Length);
					data[t] = data[random];
					data[random] = tmp;
				}
			}
			return data;
		}
		/// <summary>
		/// Fetch the correct answer for a particular question
		/// </summary>
		/// <param name="index">The index of the question (real, not scrambled, use the property instead of the array index)</param>
		/// <returns>The correct answer to the specified question</returns>
		public string GetCorrectAnswer(int index)
		{
			return this.loadedFile[index].Value;
		}
		/// <summary>
		/// Take in a bool array of scored entries, mark as hidden the ones that are correct
		/// </summary>
		/// <param name="hides">Array of entries to hide</param>
		public void Score(bool[] hides)
		{
			for (int i = 0; i < hides.Length; i++)
			{
				this.loadedFile[i].Hidden = hides[i];
			}
			this.Save();
		}
		/// <summary>
		/// Forget all hidden entries from previous tests
		/// </summary>
		public void Forget()
		{
			foreach (Entry e in this.loadedFile)
			{
				e.Hidden = false;
			}
		}
	}
}

