Soushin
=======

Soushin is a small application designed to aid in memorization. It works
a bit like a digital set of flash cards. Enter your questions and their
correct answers, and save the list. Study them, take a test, and score
your own answers correct or incorrect.

Soushin is based on memory research which shows that repeatedly studying
information you already know does not help you learn it better. Being
forced to recall information is what helps commit that information to
your long-term memory.

To this end, in Soushin's study mode, you will only see questions and
answers that you have never been tested on, or that you have been tested
on and gotten wrong in the past. 

When you take a test, you will see all questions, regardless of whether
you've gotten them right in the past.

When you score your test, the answers you mark as correct will be hidden
from your next study session, so you can focus on learning the things
you can't quite remember yet.

Soushin is written in C# using GTK# and has only been tested using Mono
on Linux, though in theory it is also compatible with Windows with the
GTK# libraries installed. You can build the project using MonoDevelop.
Soushin saves question lists as XML for ease of access from other
programs.
